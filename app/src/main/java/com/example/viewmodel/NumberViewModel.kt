package com.example.viewmodel

import androidx.lifecycle.ViewModel

class NumberViewModel: ViewModel() {
    private var number: Int = 0

    fun getNumber(): Int {
        return number
    }

    fun setNumber(newNumber: Int) {
        number = newNumber
    }
}
