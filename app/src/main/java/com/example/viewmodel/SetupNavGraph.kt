package com.example.viewmodel

import androidx.compose.runtime.Composable
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable

@Composable
fun SetupNavGraph(
    navController: NavHostController,
    numberViewModel: NumberViewModel
) {
    NavHost(
        navController = navController,
        startDestination = Screen.Counter.route
    ) {
        composable(
            route = Screen.Counter.route
        ) {
            Counter(
                viewModel = numberViewModel,
                navController = navController
            )
        }

        composable(
            route = Screen.ResetCounter.route
        ) {
            ResetCounter(viewModel = numberViewModel)
        }
    }
}