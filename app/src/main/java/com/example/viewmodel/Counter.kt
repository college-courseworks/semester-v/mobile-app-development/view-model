package com.example.viewmodel

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Button
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableIntStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavController
import androidx.navigation.compose.rememberNavController

@Composable
fun Counter(
    viewModel: NumberViewModel,
    navController: NavController
) {
    var number by remember { mutableIntStateOf(viewModel.getNumber()) }

    Column(
        modifier = Modifier.fillMaxSize(),
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Text(
            text = number.toString(),
            fontSize = 56.sp,
            fontWeight = FontWeight.Bold
        )

        Spacer(modifier = Modifier.height(16.dp))

        Row(
            verticalAlignment = Alignment.CenterVertically
        ) {
            Button(
                onClick = {
                    number--
                    viewModel.setNumber(number)
                },
                modifier = Modifier.padding(8.dp)
            ) {
                Text(text = "Kurang")
            }

            Button(
                onClick = {
                    number++
                    viewModel.setNumber(number)
                },
                modifier = Modifier.padding(8.dp)
            ) {
                Text(text = "Tambah")
            }
        }

        Spacer(modifier = Modifier.height(16.dp))

        Button(
            onClick = {
                navController.navigate(route = Screen.ResetCounter.route)
            },
            modifier = Modifier.padding(8.dp)
        ) {
            Text(text = "Halaman Reset")
        }
    }
}

@Preview(showBackground = true, showSystemUi = true)
@Composable
fun Counter() {
    Counter(
        viewModel = NumberViewModel(),
        navController = rememberNavController()
    )
}