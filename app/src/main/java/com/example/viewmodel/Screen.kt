package com.example.viewmodel

sealed class Screen(val route: String) {
    object Counter: Screen("counter_screen")
    object ResetCounter: Screen("reset_counter_screen")
}
